use core::{intrinsics, panic, sync};

static mut BOOTINFO: *const sel4::BootInfo = core::ptr::null();

#[panic_handler]
fn panic_handler(_: &panic::PanicInfo) -> ! {
    intrinsics::abort()
}

#[lang = "eh_personality"]
fn eh_personality() -> ! {
    intrinsics::abort()
}

#[lang = "oom"]
fn oom(_layout: core::alloc::Layout) -> ! {
    intrinsics::abort()
}

#[lang = "termination"]
trait Termination {
    fn report(self) -> i32;
}

impl Termination for () {
    fn report(self) -> i32 {
        0
    }
}

#[lang = "start"]
fn lang_start<T: Termination>(main: fn() -> T, _argc: isize, _argv: *const *const u8) -> isize {
    main();

    intrinsics::abort();
}

#[no_mangle]
unsafe extern "C" fn __sel4_start_init_boot_info(bootinfo: *const sel4::BootInfo) {
    // stash away the bootinfo
    BOOTINFO = bootinfo;
}

pub fn get_boot_info() -> (&'static sel4::BootInfo, sel4::IPCBuffer) {
    use sync::atomic::{AtomicBool, Ordering};

    static RUN_ONCE: AtomicBool = AtomicBool::new(false);

    if !RUN_ONCE.swap(true, Ordering::Relaxed) {
        unsafe {
            (
                BOOTINFO
                    .as_ref()
                    .expect("BootInfo was not set by the boot stub"),
                sel4::IPCBuffer::from_raw((*BOOTINFO).ipc_buffer),
            )
        }
    } else {
        panic!("get_boot_info was called more than once, it can only be called once");
    }
}

#[cfg(feature = "CONFIG_ARCH_X86_64")]
global_asm!(
    r#"
/* Copyright (c) 2017 The Robigalia Project Developers
 * Licensed under the Apache License, Version 2.0
 * <LICENSE-APACHE or
 * http://www.apache.org/licenses/LICENSE-2.0> or the MIT
 * license <LICENSE-MIT or http://opensource.org/licenses/MIT>,
 * at your option. All files in the project carrying such
 * notice may not be copied, modified, or distributed except
 * according to those terms.
 */
.global _sel4_start
.global _start
.global _stack_bottom
.text

_start:
_sel4_start:
    leaq    _stack_top, %rsp
    /* Setup the global "bootinfo" structure. */
    call    __sel4_start_init_boot_info

    /* N.B. rsp MUST be aligned to a 16-byte boundary when main is called.
     * Insert or remove padding here to make that happen.
     */
    pushq $0
    /* Null terminate auxv */
    pushq $0
    pushq $0
    /* Null terminate envp */
    pushq $0
    /* add at least one environment string (why?) */
    leaq environment_string, %rax
    pushq %rax
    /* Null terminate argv */
    pushq $0
    /* Give an argv[0] (why?) */
    leaq prog_name, %rax
    pushq %rax
    /* Give argc */
    pushq $1
    /* No atexit */
    movq $0, %rdx

    /* Now go to the "main" stub that rustc generates */
    call main

    /* if main returns, die a loud and painful death. */
    ud2

    .data
    .align 4

environment_string:
    .asciz "seL4=1"
prog_name:
    .asciz "rootserver"

    .bss
    .align  4096
_stack_bottom:
    .space  65536
_stack_top:
"#,
    options(att_syntax)
);

#[cfg(feature = "CONFIG_ARCH_AARCH64")]
global_asm!(
    r#"
/* Copyright (c) 2020 The Robigalia Project Developers
 * Licensed under the Apache License, Version 2.0
 * <LICENSE-APACHE or
 * http://www.apache.org/licenses/LICENSE-2.0> or the MIT
 * license <LICENSE-MIT or http://opensource.org/licenses/MIT>,
 * at your option. All files in the project carrying such
 * notice may not be copied, modified, or distributed except
 * according to those terms.
 */
.global _sel4_start
.global _start
.global _stack_bottom
.text

_start:
_sel4_start:
    ldr x19, =_stack_top
    mov sp, x19
    /* x0, the first arg in the calling convention, is set to the bootinfo
     * pointer on startup. */
    bl __sel4_start_init_boot_info
    /* zero argc, argv */
    mov x0, #0
    mov x1, #0
    /* Now go to the "main" stub that rustc generates */
    bl main

.pool
    .data
    .align 8
    .bss
    .align  8
_stack_bottom:
    .space  65536
_stack_top:
"#
);
